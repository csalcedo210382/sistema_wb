<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');


class Service
{
	public $db;
	public $idUsuario = 0;
	public $fechaCreacion;
	private $inicio = false;
	private $excels_dir;




	function __construct() 
	{
		//if(!PRODUCTION_SERVER)
			$this->_iniciar();
			
        	if(PRODUCTION_SERVER) $this->db->hide_errors();
    		$this->excels_dir = PHPDIR.'../excels' ;
	}
	function _iniciar(){
		if($this->inicio)
			return;
		$this->inicio = true;
		$GLOBALS['amfphp']['encoding'] = 'amf3';
		$this->db = new ezSQL_mysql(DB_USER,DB_PASS,DB_NAME,DB_HOST);
		if(PRODUCTION_SERVER) 
			$this->db->hide_errors();
		else{
			$this->idUsuario = 1;		

		}
		$this->fechaCreacion = uc_sqlDate(ZONA_HORARIA);
	}
	function _codificarObjeto($dato,$array){
		if($dato == null){
			return null;
		}
		else if($array == null || COUNT($array) == 0){
			return $dato;
		}
		else if(is_array($dato)){
			foreach ($dato as $value) {
				foreach ($array as $val) {
					$value->{$val} = $this->_codificarPalabra($value->{$val});
				}
			}
		}
		else{
			foreach ($array as $val) {
				$dato->{$val} = $this->_codificarPalabra($dato->{$val});
			}
		}
	}
	
	function _getAccessToken($at=null){
		if($_SESSION["at"] == null){
			if($at)
				$sql = "SELECT fbUser,access_token FROM access_token WHERE access_token!='$at' AND access_token != '' ORDER BY usado ASC LIMIT 1";
			else
				$sql = "SELECT fbUser,access_token FROM access_token WHERE access_token != '' ORDER BY usado ASC LIMIT 1";
			$res = $this->db->get_row($sql);
			$at = $res->access_token;
			$fbUser = $res->fbUser;
			$sql = "UPDATE access_token SET usado = usado + 1 WHERE fbUser = '$fbUser' ";
			$this->db->query($sql);
			$_SESSION["at"] = $at;
		}
		return $_SESSION["at"];
	}
	

	function _leerUrl($url,$data=null,$tipo=""){
		$ch = curl_init();
		if($tipo == "GET"){

		}
		else if($tipo == "POST"){
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, COUNT($data));
			curl_setopt($ch, CURLOPT_POST,true);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		}
		else{
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		}
		$result = curl_exec($ch);
		return $result;
	}
	function _runSuperQuery($sql,$db){
		$array = explode(";",$sql);
		foreach($array as $d){
			try{
				$db->query($d);
			}
			catch(Exception $e){

			}
		}
		return "termino";
	}
	
	function _decodificarPalabra($label){
		return $this->db->escape(mb_check_encoding($label,'UTF-8')?utf8_decode($label):$label);
	}
	function _codificarPalabra($label){
		return mb_check_encoding ( $label ,  'UTF-8' )  ? $label : utf8_encode ( $label);
	}
	function _fechaTimeToDate($tiempo){
		$cd = $this->getStringForNumber($tiempo);
		$cd = new DateTime("@$cd");
		return $this->utcToLocalTime($cd->format('Y-m-d H:i:s' ));
	}
	function _utcToLocalTime( $utcTime ){
		$date = new DateTime($utcTime,new DateTimeZone('UTC'));
		$date->setTimezone(new DateTimeZone(ZONA_HORARIA));
		return $date->format('Y-m-d H:i:s');
	}
	function _getStringForNumber($string){
		return number_format($string, 0, '', '');
	}
	
	function _clave_aleatoria(){ 
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$len = rand(20,strlen($str)-1);
		$cad = "";
		for($i=0;$i<$len;$i++) {
			$cad .= substr($str,rand(0,62),1);
		}
	    $date = new DateTime();
	    $date = $date->getTimestamp();
	    $str = $date."-".$cad;
	    return $str;
	}
	function _validarLlave($llave){
		$sql = "SELECT idSession,idCliente FROM session_cliente WHERE estado = 1 AND llave = '$llave'";
		$dato = $this->db->get_row($sql);
		if($dato == null)
			return false;
		$idSession = $dato->idSession;
		$idAdmin = $dato->idAdmin;
		$sql = "UPDATE session SET conexiones = conexiones + 1 WHERE idSession = $idSession";
		$this->db->query($sql);
		$this->direccion = "";
		return true;
	}
	function _limpiarPalabra($texto){
      	$textoLimpio = strtolower(preg_replace('([^A-Za-z0-9])', '', $texto));	     					
      	return $textoLimpio;
	}
	function _getSoloNumero($texto){
		$result = "";
		$i = 0;
		while(strlen($texto)>$i){
			$letra = substr($texto,$i,1);			
			if(is_numeric( $letra )){
				$result .= $letra;
			}
			$i++;
		}
		return $result;
	}

	public function getTotalRegistros($variable,$tabla){
		$sql = "SELECT count($variable) FROM $tabla";
		$res = $this->db->get_var($sql);
		return $res;
	}

	public function getVarTabla($variable,$tabla){
		$sql = "SELECT $variable FROM $tabla";
		$res = $this->db->get_var($sql);
		return $res;
	}

	public function getResultados($campos,$tabla){
		$sql = "SELECT $campos FROM $tabla";
		$res = $this->db->get_results($sql);
  		return $res;
	}

	public function getDato($variable,$tabla,$condicion){
		$sql = "SELECT $variable FROM $tabla WHERE $condicion";
		$res = $this->db->get_var($sql);
		return $res;
	}




}
?>
