<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once("Service.php");

class ServiceAdministrativo extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

//PARTE INICIAL DE ADMINISTRATIVO	

	function getMenu($permisos){

		$sql = "SELECT idPerfil,enlace,titulo FROM permisos WHERE idPerfil IN ($permisos) ORDER BY idPerfil ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getControles($permisos){

		$sql = "SELECT distinct control FROM permisos WHERE idPerfil IN ($permisos) ORDER BY idPerfil ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function archivosPendientes(){

		$total_archivos = count(glob('archivos_sistema/archivos_pendientes/{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function conflictoBarras(){

		$sql = "UPDATE captura C SET C.sku_cap = ( SELECT M.sku_barra FROM maestro M WHERE M.cod_barra = C.barra_cap LIMIT 1)";
		$res=$this->db->query($sql);

		$sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_var($sql_consulta);
		return $res_consulta;

	}

	function listarConflictoBarras(){

		$sql_consulta = "SELECT area_cap,barra_cap,cant_cap FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_results($sql_consulta);
		return $res_consulta;

	}

	function getAvanceCaptura(){
		$sqlRangos = "SELECT * FROM area_rango ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("ubicacion","justificacion"));

		$resultado = new stdClass();
        $resultado->rangos = $resRangos;
        $resultado->capturas = $resCaptura;
        $resultado->justificados = $resJustificados;

		return $resultado;
	}


}	
?>