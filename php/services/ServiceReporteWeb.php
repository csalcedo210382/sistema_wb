<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceReporteWeb extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function getDepartamentos($cliente,$tienda,$fecha){
		$sqlDep = "SELECT departamento_original departamento FROM diferencia_total
				WHERE cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'
				ORDER BY departamento ASC";
		$resDep = $this->db->get_results($sqlDep);
		$this->_codificarObjeto($resDep,array("departamento"));

        return $resDep;
	}

	function getReporteDiferencias($cliente,$tienda,$fecha){
		$sql = "SELECT * FROM diferencia_total
				WHERE cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'
				ORDER BY diferencia_soles DESC";
		$resDiferencia = $this->db->get_results($sql);
		$this->_codificarObjeto($resDiferencia,array("departamento","departamento_original"));

		$sql = "SELECT * FROM avance
				WHERE cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'
				ORDER BY idAvance ASC";
		$resAvance = $this->db->get_results($sql);
		$this->_codificarObjeto($resAvance,array("departamento"));

		$resultado = new stdClass();
        $resultado->diferencia = $resDiferencia;
        $resultado->avance = $resAvance;

        return $resultado;
	}

	function getReporteCaptura($cliente,$tienda,$fecha){
		$sqlCap = "SELECT area_rango, avance FROM avance_total
				WHERE cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'";
		$resCap = $this->db->get_results($sqlCap);
		$this->_codificarObjeto($resCap,array("area_rango"));

        return $resCap;
	}

	function getReporteSubDiferencias($departamento,$cliente,$tienda,$fecha){
		$departamento = utf8_decode($departamento);
		$sqlDepartamento = "SELECT * FROM sub_diferencia_total
				WHERE departamento = '$departamento' AND cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'
				AND (diferencia_soles > 0 OR diferencia_cantidad > 0)
				ORDER BY diferencia_soles DESC";
		$resDepartamento = $this->db->get_results($sqlDepartamento);
		$this->_codificarObjeto($resDepartamento,array("departamento","sub_departamento"));

		$sqlRanking = "SELECT * FROM ranking_productos
				WHERE departamento = '$departamento' AND cliente = '$cliente' AND tienda = '$tienda' AND fecha = '$fecha'";
		$resRanking = $this->db->get_results($sqlRanking);
		$this->_codificarObjeto($resRanking,array("sku_stk","des_sku_stk","departamento","sub_departamento","producto"));

		$resultado = new stdClass();
        $resultado->departamento = $resDepartamento;
        $resultado->ranking = $resRanking;

        return $resultado;
	}

	function getTiendasCliente($cliente){
		$sql = "SELECT * FROM tienda
				WHERE cliente = '$cliente'
				ORDER BY numeroTienda ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreTienda"));
		return $res;
	}

	function getFechaInventario($cliente,$tienda){
		$sql = "SELECT DISTINCT fecha FROM diferencia_total
				WHERE cliente = '$cliente' AND tienda = '$tienda'
				ORDER BY fecha ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getArchivosGenerados($cliente,$tienda,$fecha){
		$ncliente = $cliente;
		$ntienda = $tienda;
		$caracteres = array('-', ':');
		$reemplazos = array('_', '_');
		$nfecha = str_replace($caracteres, $reemplazos, $fecha);
		$archivo = "archivos_generados_".$ncliente."".$ntienda."".$nfecha.".zip";
		$nombre_fichero = "../archivos_sistema/".$archivo;
		$retorno = 0;

		if (file_exists($nombre_fichero)) {

			$bytes = filesize($archivo);
	        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
	        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
	        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
	    	$conteo = count(file($archivo));

	        $file = new stdClass();
	        $file->nombre = $archivo;
	        $file->filas = ($conteo - 1);
	        $file->peso = $peso;
	        $file->fecha = date("Y-m-d", filectime($archivo));

	        $archivos[] = $file;

		    $retorno = $archivos;
		} else {
		    $retorno = 0;
		}

		return $retorno;
	}



}	
?>