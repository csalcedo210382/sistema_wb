<?php

define('ZONA_HORARIA','America/Lima');

define("PRODUCTION_SERVER", false);

if(PRODUCTION_SERVER){
	
	define('CANVAS_URL', 'http://www.igroupsac.com/sistema' );
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');
}else{
	define('CANVAS_URL', 'http://localhost/sistema_wb' );
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');
}


//--
//-- AMFPHP stuff
//--

define('AMFCORE', INCDIR.'amfphp/core/'); //  directorio de las clases de amfphp
define('SRVPATH', PHPDIR.'services/'); // carpeta donde estan los servicios del amfphp
define('VOPATH', PHPDIR.'services/vo/'); // carpeta donde estan los vo que usan los servicios

//--
//-- MySQL stuff
//--

if(PRODUCTION_SERVER)
{

	define("DB_HOST", "mysql.igroupsac.com");
	define("DB_USER", "user_igroupsac");
	define("DB_PASS", "User210382!?");
	define("DB_NAME", "database_igroupsac");

}
else
{
	
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", "");
	define("DB_NAME", "sistema_wb");
}

//--
//-- Log stuff
//--

// la carpeta logs debe tener permisos de escritura si se va utilizar un archivo de log
define('LOG_DIR', PHPDIR.'logs' );
define('LOG_BASE', 'log_app' );

?>