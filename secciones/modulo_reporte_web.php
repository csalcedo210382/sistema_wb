                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="reporte_web">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2 texto">REPORTE WEB</div>
                                <div class="col-md-8"></div>
                                <div class="col-md-2 derecha texto">
                                    <!--<a href="#" title="Actualizar lista" onclick="actualizarModuloSolicitud();return false;" style="color:#fff;"><span class="glyphicon glyphicon-refresh"></span> ACTUALIZAR</a>-->
                                </div>
                            </div>
                            
                        </div>
                        <div class="panel-body">
                                <!-- Nav tabs -->
                                <br>
                                        <div class="row">
                                            <div class="col-md-1 texto negrita derecha">TIENDA</div>
                                            <div class="col-md-2">
                                                <select class="form-control input-sm nombreTienda">
                                                    <option value=''>---SELECCIONAR---</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1 texto negrita derecha textoFechaInventario">FECHA</div>
                                            <div class="col-md-2">
                                                <select class="form-control input-sm fechaInventario">
                                                    <option value=''>---SELECCIONAR---</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1 texto negrita derecha textoNombreDepartamento">DEPARTAMENTO</div>
                                            <div class="col-md-2">
                                                <select class="form-control input-sm nombreDepartamento">
                                                    <option value='0'>---SELECCIONAR---</option>
                                                </select>
                                            </div>
                                        </div>
                                <br>


                                <!--<div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4">
                                        <button id="segmento_diferencias" type="button" href="#diferencias" data-toggle="tab" class="btn btn-info btn-sm btn-block botonGrande">DIFERENCIAS</button>
                                    </div>
                                    <div class="col-md-4">
                                        <button id="segmento_captura" type="button" href="#capturas" data-toggle="tab" class="btn btn-warning btn-sm btn-block botonGrande">AVANCE DE CAPTURA</button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>-->


                                
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a id="segmento_diferencias" href="#diferencias" data-toggle="tab" aria-expanded="false">DIFERENCIAS</a>
                                    </li>
                                    <li><a id="segmento_captura" href="#capturas" data-toggle="tab" aria-expanded="false">AVANCE DE CAPTURA</a>
                                    </li>
                                    <li><a id="segmento_archivos" href="#archivos" data-toggle="tab" aria-expanded="false">ARCHIVOS GENERADOS</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <BR>
                                    <div class="tab-pane fade active in" id="diferencias">
                                        
                                        <div class="row departamento">
                                            <div class="col-md-12 texto negrita centro">REPORTE DE DIFERENCIAS</div>
                                        </div>
                                        <BR>

                                        <div class="row departamento">

                                            <div class="col-md-12">
                                                <table id="tablaAvanceGeneral" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVANCE GENERAL</th>
                                                            <th colspan="2">UNIDADES</th>
                                                            <th colspan="2">SOLES S/.</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr>
                                                            <td rowspan="3" class="negrita grande avancePorcentajeTotal">0 %</td>
                                                            <td class="negrita">CLIENTE</td>
                                                            <td class="stockClienteTotal">0</td>
                                                            <td class="negrita">CLIENTE</td>
                                                            <td class="solesClienteTotal">0</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="negrita">IGROUP</td>
                                                            <td class="cantidadIgroupTotal">0</td>
                                                            <td class="negrita">IGROUP</td>
                                                            <td class="solesIgroupTotal">0</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="negrita">DIFER.</td>
                                                            <td class="diferenciaCantidadTotal">0</td>
                                                            <td class="negrita">DIFER.</td>
                                                            <td class="diferenciaSolesTotal">0</td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                        <div class="row departamento">
                                            <div class="col-md-12 texto negrita">GRAFICO POR DEPARTAMENTO</div>
                                        </div>
                                        <div class="ventana_barra departamento">
                                            <div id="gra_dif_dep" class="grafico_departamento">
                                                
                                            </div>
                                        </div>
                                        <BR>
                                        <div class="row subdepartamento">
                                            <div class="col-md-12 texto centro negrita">REPORTE POR SUB DEPARTAMENTOS</div>
                                        </div>
                                        <div class="sub_ventana_barra subdepartamento">
                                            <div id="gra_dif_sub_dep" class="grafico_subdepartamento">
                                                
                                            </div>
                                        </div>
                                        <BR>
                                        <div class="row subdepartamento">
                                            <div class="col-md-12 texto centro negrita">RANKING DE PRODUCTOS</div>
                                        </div>
                                        <div class="ventana_ranking subdepartamento">
                                            <div class="tabla_ranking">

                                                <table id="tablaRankingProductos" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>SKU</th>
                                                            <th>SKU DESCRIPCIÓN</th>
                                                            <th>COSTO</th>
                                                            <th>STOCK</th>
                                                            <th>CONTADO</th>
                                                            <th>DIF CANTIDAD</th>
                                                            <th>DIF SOLES</th>
                                                            <th>SUB DEPARTAMENTO</th>
                                                            <th>PRODUCTO</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                       
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>                                            


                                    </div>

                                    <div class="tab-pane fade" id="capturas">
                                        
                                        <div class="row">
                                            
                                            <div class="col-md-12 texto negrita centro avance">REPORTE DE AVANCE DE CAPTURA</div>

                                        </div>
                                        <BR>
                                        <div class="ventana_avance avance">
                                            <div id="gra_ava" class="grafico_avance">
                                                
                                            </div>
                                        </div>


                                    </div>


                                    <div class="tab-pane fade" id="archivos">
                                        
                                        <div class="row">
                                            
                                            <div class="col-md-12 texto negrita centro avance">ARCHIVOS GENERADOS</div>

                                        </div>
                                        <BR>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="tablaListaArchivosGenerados" width="100%" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">#</th>
                                                            <th width="55%">ARCHIVO</th>
                                                            <th width="10%">FECHA</th>
                                                            <th width="10%">TAMAÑO</th>
                                                            <th width="20%">OPCIONES</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        </tr>                                              
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                      
                        </div>                        



                    </div>
                </div>

