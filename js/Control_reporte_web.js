var service = new Service("webService/index.php");


$(function() 
{

iniciarControlReporteWeb();

});

function iniciarControlReporteWeb(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");

    $("#menu_reporte_web").on('click',function(){
        //service.procesar("getReporteDiferencias",tienda,resultadoReporteDiferencia);
        service.procesar("getTiendasCliente",usuario,resultadoTiendasCliente);
    })



    $(".departamento").hide();
    $(".subdepartamento").hide();
    $(".avance").hide();


    $(".textoFechaInventario").hide();
    $(".fechaInventario").hide();
    $(".textoNombreDepartamento").hide();
    $(".nombreDepartamento").hide();
    
    function resultadoTiendasCliente(evt){
        resultado = evt;
        $("#reporte_web .nombreTienda").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#reporte_web .nombreTienda" ).append( "<option value='"+ resultado[i].numeroTienda +"'>"+ resultado[i].nombreTienda +"</option>" );
        }
    }

    $("#reporte_web .nombreTienda").on('change', function(){
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        service.procesar("getFechaInventario",cliente,tienda,resultadoFechaInventario);
    })

    function resultadoFechaInventario(evt){
        resultado = evt;
        $("#reporte_web .fechaInventario").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ){
            $(".textoFechaInventario").hide();
            $(".fechaInventario").hide();
            $(".textoNombreDepartamento").hide();
            $(".nombreDepartamento").hide();
            $(".departamento").hide();
            $(".subdepartamento").hide();
            $(".avance").hide();
            return;
        }
        $(".textoFechaInventario").show();
        $(".fechaInventario").show();
        for(var i=0; i<resultado.length ; i++){
            $("#reporte_web .fechaInventario").append( "<option value='"+ resultado[i].fecha +"'>"+ resultado[i].fecha +"</option>" );
        }
    }

    $("#reporte_web .fechaInventario").on('change', function(){
        procesarReporteDiferencia();
        procesarDepartamentos();
        procesarArchivosGenerados();
    })

    $("#segmento_diferencias").on('click', function(){
        procesarReporteDiferencia();
        procesarDepartamentos();
    })

    $("#segmento_captura").on('click', function(){
        procesarReporteCaptura();
        graficoAvanceTotal();
    })

    function procesarDepartamentos(){
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        var fecha = $("#reporte_web .fechaInventario").val();
        service.procesar("getDepartamentos",cliente,tienda,fecha,resultadoDepartamentos);   
    }

    function procesarReporteDiferencia(){
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        var fecha = $("#reporte_web .fechaInventario").val();
        service.procesar("getReporteDiferencias",cliente,tienda,fecha,resultadoReporteDiferencia);   
    }

    function procesarArchivosGenerados(){
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        var fecha = $("#reporte_web .fechaInventario").val();
        service.procesar("getArchivosGenerados",cliente,tienda,fecha,function(evt){
            if(evt != 0){


                resultado = evt;
                $("#tablaListaArchivosGenerados tbody").html("");
                if ( resultado == undefined ) return;
                for(var i=0; i<resultado.length ; i++){
                    datoRegistro = resultado[i];
                    var fila = $("<tr>");
                    var celdaBotones = $("<td>");
                    //(i + 1)
                    fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>');
                    var contenedorBotones = $("<td>");
                    var btnInfo = $("<button class='btn btn-info btn-sm' title='VER LOG'>");
                    var btnRemover = $("<button class='btn btn-danger btn-sm' title='EIMINAR'>");
                    btnInfo.html('<span class="glyphicon glyphicon-eye-open"></span> ARCHIVO ZIP');
                    btnRemover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR ARCHIVO');
                    btnInfo.data("data",datoRegistro);
                    btnRemover.data("data",datoRegistro);
                    btnInfo.on("click",infoArchivosGenerados);
                    btnRemover.on("click",removerArchivosGenerados);
                    contenedorBotones.append(btnInfo);
                    contenedorBotones.append(" ");
                    contenedorBotones.append(btnRemover);
                    fila.append(contenedorBotones);
                    $("#tablaListaArchivosGenerados tbody").append(fila);
                }


            }else{

                $("#tablaListaArchivosGenerados tbody").html("");

            }
        }); 
    }

    function infoArchivosGenerados(){
        var data = $(this).data("data");
        window.open("archivos_sistema/"+data.nombre, '_blank');
    }

    function removerArchivosGenerados(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoGenerados",data.nombre,mensajeEliminarArchivosGenerados);
            }
        });
    }

    function mensajeEliminarArchivosGenerados(evt){
        if(evt > 0){
            service.procesar("listarArchivosGenerados",cargaListarArchivosGenerados);
            alertify.success("REGISTRO ELIMINADO CORRECTAMENTE");
        }else{
            alertify.error("REGISTRO NO ELIMINADO");
        }

    }


    function procesarReporteCaptura(){
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        var fecha = $("#reporte_web .fechaInventario").val();
        service.procesar("getReporteCaptura",cliente,tienda,fecha,resultadoReporteCaptura);   
    }

    function resultadoDepartamentos(evt){
        var valorActual = $("#reporte_web .nombreDepartamento").val();
        resultado = evt
        $("#reporte_web .nombreDepartamento").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var x=0; x<resultado.length; x++){
            $("#reporte_web .nombreDepartamento" ).append( "<option value='"+ resultado[x].departamento +"'>"+ resultado[x].departamento +"</option>" );
        }
        $("#reporte_web .nombreDepartamento").val(valorActual);
    }

    function resultadoReporteDiferencia(evt){
        grafico = [];
        departamentoGrafico = [];
        solesGrafico = [];
        cantidadGrafico = [];
        resultado = evt.diferencia;
        avance = evt.avance;
        if ( resultado == undefined ){
            $(".departamento").hide();
            $(".subdepartamento").hide();
            $(".avance").hide();
            $(".textoNombreDepartamento").hide();
            $(".nombreDepartamento").hide();
            return;
        }
        $(".departamento").show();
        $(".textoNombreDepartamento").show();
        $(".nombreDepartamento").show();

        $("#diferencias .avancePorcentajeTotal").html(formatNumber.new( parseFloat(avance[0].porcentaje).toFixed(2), "") + ' %');
        $("#diferencias .stockClienteTotal").html(formatNumber.new( parseFloat(avance[0].stock).toFixed(2), "") );
        $("#diferencias .cantidadIgroupTotal").html(formatNumber.new( parseFloat(avance[0].captura).toFixed(2), "") );
        $("#diferencias .solesClienteTotal").html(formatNumber.new( parseFloat(avance[0].stock_sol).toFixed(2), "") );
        $("#diferencias .solesIgroupTotal").html(formatNumber.new( parseFloat(avance[0].captura_sol).toFixed(2), "") );
        $("#diferencias .diferenciaCantidadTotal").html(formatNumber.new( parseFloat(avance[0].captura - avance[0].stock).toFixed(2), "") );
        $("#diferencias .diferenciaSolesTotal").html(formatNumber.new( parseFloat(avance[0].captura_sol - avance[0].stock_sol).toFixed(2), "") );

        for(var i=0; i<resultado.length ; i++){
            departamentoGrafico.push(resultado[i].departamento);
            solesGrafico.push(resultado[i].diferencia_soles);
            cantidadGrafico.push(resultado[i].diferencia_cantidad);
        }

        grafico.push(departamentoGrafico);
        grafico.push(solesGrafico);
        grafico.push(cantidadGrafico);

        
        setTimeout(function(){
            graficoDiferencias(grafico);
        }, 1000);

    }

    function resultadoReporteCaptura(data_grafico){
        grafico = [];
        arearangoGrafico = [];
        porcentajeGrafico = [];
        resultado = data_grafico;
        ancho_grafico = "125em";
        registros = 0;
        if ( resultado == undefined ){
            $(".avance").hide();
            return;
        }
        $(".avance").show();

        for(var i=0; i<resultado.length ; i++){
            registros = registros + 1;
            arearangoGrafico.push(resultado[i].area_rango);
            porcentajeGrafico.push(resultado[i].avance);
        }

        
        if(registros > 10){
            ancho_grafico = (parseFloat(registros) * 12) + "em";
        }
        $(".grafico_avance").css("width",ancho_grafico);

        grafico.push(arearangoGrafico);
        grafico.push(porcentajeGrafico);

        setTimeout(function(){
            graficoAvance(grafico);
        }, 1000);

    }

    $("#reporte_web .nombreDepartamento").on('change', function(){
        procesarSubTotales();
    })

    function procesarSubTotales(){
        var departamento = $("#reporte_web .nombreDepartamento").val();
        var cliente = usuario;
        var tienda = $("#reporte_web .nombreTienda").val();
        var fecha = $("#reporte_web .fechaInventario").val();
        service.procesar("getReporteSubDiferencias",departamento,cliente,tienda,fecha,resultadoReporteSubDiferencia);
    }

    function resultadoReporteSubDiferencia(evt){
        grafico = [];
        departamentoGrafico = [];
        solesGrafico = [];
        cantidadGrafico = [];
        resultado = evt.departamento;
        registros = 0;
        ancho_grafico = "125em";
        //ancho_grafico = "125em";
        if ( resultado == undefined ){
            $(".subdepartamento").hide();
            return; 
        }
        $(".subdepartamento").show();
        //$(".grafico_subdepartamento").css("height","30em");


        for(var i=0; i<resultado.length ; i++){
            registros = registros + 1;
            departamentoGrafico.push(resultado[i].sub_departamento);
            solesGrafico.push(resultado[i].diferencia_soles);
            cantidadGrafico.push(resultado[i].diferencia_cantidad);
        }

        if(registros > 5){
            ancho_grafico = (parseFloat(registros) * 25) + "em";
        }
        $(".grafico_subdepartamento").css("width",ancho_grafico);


        grafico.push(departamentoGrafico);
        grafico.push(solesGrafico);
        grafico.push(cantidadGrafico);

        setTimeout(function(){
            graficoSubDiferencias(grafico);
        }, 1000);

        //RANKING PRODUCTOS
        ranking = evt.ranking;
        $("#tablaRankingProductos tbody").html("");
        if ( ranking == undefined ) return;

        for(var i=0; i<ranking.length ; i++){
            var fila = $("<tr>");
            fila.html('<td>'+ (i + 1) +'</td><td>'+ ranking[i].sku_stk +'</td><td>'+ ranking[i].des_sku_stk +'</td><td>'+ ranking[i].costo +'</td><td>'+ ranking[i].stock +'</td><td>'+ ranking[i].contado +'</td><td>'+ formatNumber.new( parseFloat(ranking[i].dif_cant).toFixed(2), "") +'</td><td>'+ formatNumber.new( parseFloat(ranking[i].dif_sol).toFixed(2), "") +'</td><td>'+ ranking[i].sub_departamento +'</td><td>'+ ranking[i].producto +'</td>');
            $("#tablaRankingProductos tbody").append(fila);
        }

    }



    function graficoDiferencias(evt) {

        departamento = evt[0];
        soles = evt[1];
        cantidad = evt[2];

        //console.log(evt);

        $("#gra_dif_dep").kendoChart({
                legend: {
                    position: "top"
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "SOLES",
                    data: soles,
                    labels: {
                        visible: true,
                        template: "S/. #= kendo.toString(kendo.parseInt(value), 'N0') #"
                    }
                }, {
                    name: "CANTIDAD",
                    data: cantidad,
                    labels: {
                        template: "#= kendo.toString(kendo.parseInt(value), 'N0') #",
                        visible: true
                    }
                }],
                valueAxis: {
                    labels: {
                        format: "{0:N0}"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: 0
                },
                categoryAxis: {
                    categories: departamento,
                    line: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: false,
                    format: "{0}",
                    template: "#= series.name #: #= value #"
                }
            });
    }

    function graficoSubDiferencias(evt) {

        departamento = evt[0];
        soles = evt[1];
        cantidad = evt[2];

        //console.log(evt);

        $("#gra_dif_sub_dep").kendoChart({
                legend: {
                    position: "top"
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "SOLES",
                    data: soles,
                    labels: {
                        visible: true,
                        template: "S/. #= kendo.toString(kendo.parseInt(value), 'N0') #"
                    }
                }, {
                    name: "CANTIDAD",
                    data: cantidad,
                    labels: {
                        template: "#= kendo.toString(kendo.parseInt(value), 'N0') #",
                        visible: true
                    }
                }],
                valueAxis: {
                    labels: {
                        format: "{0:N0}"
                    },
                    line: {
                        visible: false
                    },
                    axisCrossingValue: 0
                },
                categoryAxis: {
                    categories: departamento,
                    line: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: false,
                    format: "{0}",
                    template: "#= series.name #: #= value #"
                }
            });
    }


    function graficoAvance(evt) {
        area_rango = evt[0];
        porcentaje = evt[1];

        //console.log(evt);

        $("#gra_ava").kendoChart({
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "AVANCE",
                    data: porcentaje,
                    labels: {
                        template: "${value} %",
                        visible: true
                    }
                }],
                valueAxis: {
                    line: {
                        visible: true
                    },
                    minorGridLines: {
                        visible: true
                    }
                },
                categoryAxis: {
                    categories: area_rango,
                    majorGridLines: {
                        visible: false
                    },
                    labels: {
                        rotation: 90
                    }
                },
                tooltip: {
                    visible: false,
                    template: "#= series.name #: #= value #"
                }
        });
    }

    function graficoAvanceTotal(evt) {

        setTimeout(function(){

            valor = 55;

            //console.log(evt);

            $("#gra_ava_tot").kendoArcGauge({
                value: valor,
                centerTemplate: '#: value #%'
            });

        }, 1000);

    }


    $("#segmento_archivos").on('click', function(){
        procesarArchivosGenerados();
    })




    var formatNumber = {
     separador: ",", // separador para los miles
     sepDecimal: '.', // separador para los decimales
     formatear:function (num){
      num +='';
      var splitStr = num.split(',');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft  +splitRight;
     },
     new:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
     }
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


