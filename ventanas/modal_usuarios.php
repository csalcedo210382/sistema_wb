<div class="modal fade" tabindex="-1" role="dialog" id="modal_usuarios">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="areaImpresionUsuarios">
                <div class="list-group">
                    
                    <div class="row">
                        <div class="col-md-2"><img src="img/logoReporte.png" height="39" width="87"></div>
                        <div class="col-md-8"></div>
                        <div class="col-md-2"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table id="tablaResultadoFiltroFormularioUsuarios" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>DNI</th>
                                        <th>COLABORADOR</th>
                                        <th>TIPO</th>
                                        <th>ESTADO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->